# Recommended approach to using bibtex: http://www.bibtex.org/Using/
latex -interaction=batchmode $1
bibtex $1
latex -interaction=batchmode $1
latex -interaction=batchmode $1
# Generate PDF
pdflatex -interaction=batchmode $1
# Move final submission files to output folder
mv *.cls output
mv *.bst output
mv ${1}.tex output
mv ${1}.bib output
mv ${1}.bbl output
mv ${1}.pdf output
