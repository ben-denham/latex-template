docker-image = latex-template

docker-build:
	docker build -t ${docker-image} -f Dockerfile.txt .
set-executable:
	chmod +x build.sh
	git update-index --assume-unchanged build.sh
generate: set-executable docker-build
	docker run -v $(shell pwd)/output:/usr/local/src/output ${docker-image}
bash: docker-build
	docker run -ti -v $(shell pwd)/output:/usr/local/src/output ${docker-image} /bin/bash
